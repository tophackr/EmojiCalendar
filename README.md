## 📅 Emoji Calendar
[![Emoji Calendar picture](https://gitlab.com/tophackr/EmojiCalendar/uploads/e892feded3ce72b04799f52f26ad360f/emojicalendar.png)](https://📅.ml)

## Contact
If you have any problems, you can [open a new issue](https://gitlab.com/tophackr/EmojiCalendar/issues/new) or write an [email](mailto:tophackr@icloud.com).

## Copyright and license
Code copyright 2020 the [Emoji Calendar authors](https://gitlab.com/tophackr/EmojiCalendar/-/graphs/master). Code released under the [MIT License](https://gitlab.com/tophackr/EmojiCalendar/blob/master/LICENSE).